public class Main {
    public static void main(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree();

        binarySearchTree.insert(5);
        binarySearchTree.insert(3);
        binarySearchTree.insert(2);
        binarySearchTree.insert(4);
        binarySearchTree.insert(6);
        binarySearchTree.insert(7);
        binarySearchTree.insert(8);

//                  5
//
//          3              7
//
//      2       4      6       8

        System.out.println("In Order : ");
        binarySearchTree.inOrder();

        System.out.println("\n\nPost Order : ");
        binarySearchTree.postOrder();

        System.out.println("\n\nPre Order : ");
        binarySearchTree.preOrder();

        System.out.println("\n\nIn Order with Stack : ");
        binarySearchTree.inOrderWithStack();
    }
}
